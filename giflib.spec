%define debug_package %{nil}

Name:          giflib
Version:       5.2.2
Release:       3
Summary:       A library and utilities for processing GIFs
License:       MIT
URL:           http://www.sourceforge.net/projects/giflib/
Source:        http://downloads.sourceforge.net/giflib/giflib-%{version}.tar.gz

# Move quantize.c back into libgif.so (#1750122)
Patch0: 0001-Move-quantize.c-back-into-libgif.so.patch
# Fix several defects found by Coverity scan
Patch1: 0002-Fix-several-defects-found-by-Coverity-scan.patch
# Generate HTML docs with consistent section IDs to avoid multilib difference
Patch2: 0003-Generate-HTML-docs-with-consistent-section-IDs-to-av.patch
Patch3: CVE-2021-40633.patch
Patch4: Fix-heap-buffer-overflow.patch

BuildRequires: make xmlto gcc
BuildRequires: ImageMagick
provides:      giflib-utils

%description
giflib is a library of gif images and provides utilities for processing images.

%package       devel
Summary:       files for developing programs which use the giflib library
Requires:      %{name} = %{version}-%{release}

%description   devel
development header files, libraries for programs using the giflib library.

%package       utils
Summary:       Programs for manipulating GIF format image files
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description   utils
The giflib-utils package contains various programs for manipulationg GIF
format imange files.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%make_build CFLAGS="$RPM_OPT_FLAGS -s -fPIC"

%install
%make_install PREFIX="%{_prefix}" LIBDIR="%{_libdir}"
rm -f doc/Makefile*

rm -f %{buildroot}/debugsourcefiles.list

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%doc ChangeLog NEWS README
%license COPYING
%{_libdir}/libgif.so.7*
%exclude %{_libdir}/*.a

%files devel
%defattr(-,root,root)
%doc doc/*
%{_libdir}/libgif.so
%{_includedir}/gif_lib.h

%files help
%defattr(-,root,root)
%{_mandir}/man1/gif*.*

%files utils
%{_bindir}/gif*

%changelog
* Tue Dec 17 2024 wangkai <13474090681@163.com> - 5.2.2-3
- Fix heap-buffer overflow

* Tue May 14 2024 liwenjie <liwenjie@kylinos.cn> - 5.2.2-2
- Fix CVE-2021-40633

* Thu Mar 07 2024 liweigang <izmirvii@gmail.com> - 5.2.2-1
- update to version 5.2.2

* Fri Sep 15 2023 Funda Wang <fundawang@yeah.net> - 5.2.1-7
- Fix CVE-2023-39742

* Thu Aug 25 2022 caodongxia <caodongxia@h-partners.com> -5.2.1-6
- Fix rpmbuild error

* Wed Jun 15 2022 duyiwei <duyiwei@kylinos.cn> - 5.2.1-5
- fix CVE-2022-28506

* Sat Sep 4 2021 zhanzhimin <zhanzhimin@huawei.com> - 5.2.1-4
- strip binary files

* Thu May 20 2021 liuyumeng <liuyumeng5@huawei.com> - 5.2.1-3
- Add a package named utils

* Wed Jul 29 2020 hanhui <hanhui15@huawei.com> - 5.2.1-2
- add bugfix

* Thu Jul 23 2020 hanhui <hanhui15@huawei.com> - 5.2.1-1
- update to 5.2.1

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.1.4-6
- add the require for devel

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.1.4-5
- change the path of files

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.1.4-4
- Type:cves
- ID:NA
- SUG:NA
- DESC:Add CVE patches

* Tue Sep 10 2019 Lijin Yang <yanglijin@huawei.com> - 5.1.4-3
- Package init
